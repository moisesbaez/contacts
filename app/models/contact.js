/*jslint node: true*/
"use strict";

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contactSchema = new Schema({
    userId : { type : Number, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true},
    email: String,
    website: String
});

var Contact = mongoose.model('Contact', contactSchema);
module.exports = Contact;
