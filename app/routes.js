/*jslint node: true, es5: true*/
"use strict";

var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    router = express.Router(),
    Contact = require('./models/contact');

mongoose.connect('mongodb://localhost/addressbook');

router
    .use(function (request, response, next) {
        if (!request.user) {
            request.user = { userId : 1};
        }

        next();
    })
    .use(bodyParser.json());

router
    .route('/contact')
    .get(function (request, response) {
        Contact.find({}, function (error, contacts) {
            if (error) {
                throw error;
            }

            response.json(contacts);
        });
    })
    .post(function (request, response) {
        var contact = new Contact(request.body);
        Contact.count({}, function (error, count) {
            count = count + 1;
            contact.userId = count;
            
            contact.save(function (error) {
                if (error) {
                    throw error;
                }
            });
        });
    });

router
    .param('id', function (request, response, next) {
        request.dbQuery = { userId : parseInt(request.params.id, 10) };
    })
    .route('/contact/:id')
    .get(function (request, response) {
        Contact.find(request.dbQuery, function (error, user) {
            response.json(user);
        });
    })
    .put(function (request, response) {
        var contact = request.body;
        delete contact.$promise;
        delete contact.$resolved;
        
        Contact.findOneAndUpdate(request.dbQuery, contact, function (error, user) {
            if (error) {
                throw error;
            }
            response.json(user[0]);
        });
    })
    .delete(function (request, response) {
        Contact.findOneAndRemove(request.dbQuery, function (error) {
            if (error) {
                throw error;
            }
        });
        response.json(null);
    });


module.exports = router;