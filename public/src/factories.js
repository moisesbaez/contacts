/*jslint node: true*/
/*globals angular*/
"use strict";

angular.module('ContactsApp')
    .factory('Contact', function ($resource) {
        return $resource('/router/contact/:id', { id : '@id'}, {
            'update' : { method : 'PUT' }
        });
    });