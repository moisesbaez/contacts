/*jslint node: true*/
/*globals angular*/
"use strict";

angular.module('ContactsApp', ['ui.router', 'ngResource', 'ngMessages'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('contacts', {
                url: '/contacts',
                controller: 'ListController',
                templateUrl: 'views/list.html'
            })
            .state('newContact', {
                url: '/contact/new',
                controller: 'NewController',
                templateUrl: 'views/new.html'
            });
    
        $locationProvider.html5Mode(true);
    });