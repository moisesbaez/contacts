/*jslint node: true*/
/*globals angular*/
"use strict";

angular.module('ContactsApp')
    .controller('ListController', function ($scope, Contact, $location) {
        $scope.contacts = Contact.query();
        $scope.fields = ['firstName', 'lastName'];
    
        $scope.sort = function (field) {
            $scope.sort.field = field;
            $scope.sort.order = !$scope.sort.order;
        };
    
        $scope.sort.field = 'firstName';
        $scope.sort.order = false;
    
        $scope.show = function (userId) {
            $location.url('/contact/' + userId);
        };
    })
    .controller('NewController', function ($scope, Contact, $location) {
        $scope.contact = new Contact({
            userId: null,
            firstName: [''],
            lastName: [''],
            email: [''],
            website: ['']
        });
    
        $scope.save = function () {
            if ($scope.newContact.$invalid) {
                $scope.$broadcast('record:invalid');
            } else {
                $scope.contact.$save();
                $location.url('/contacts');
            }
        };
    });