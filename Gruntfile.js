/*jslint node: true, es5: true*/
"use strict";

module.exports = function (grunt) {
    grunt.initConfig({
        watch: {
            options: {
                livereload: true
            },
            express: {
                files: ['*.js',  'app/**/*.js'],
                tasks: ['express:dev'],
                options: {
                    spawn: false
                }
            },
            public: {
                files: ['public/**/*.html', 'public/**/*.js', 'public/**/*.css']
            },
            bower: {
                files: ['public/vendor/*'],
                tasks: ['wiredep']
            }
        },
        express: {
            dev: {
                options: {
                    script: 'server.js'
                }
            }
        },
        wiredep: {
            task: {
                src: ['public/**/*.html']
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-wiredep');
    
    grunt.registerTask('server', ['express:dev', 'wiredep', 'watch']);
};