/*jslint node: true, nomen: true, es5: true*/
"use strict";

var express = require('express'),
    app = express(),
    router = require('./app/routes');

app
    .use(express.static('./public'))
    .use('/router', router);

app.get('*', function (request, response) {
    response.sendFile('public/main.html', {root: __dirname});
});

app.listen(3000, function () {
    console.log("Server running on port 3000");
});